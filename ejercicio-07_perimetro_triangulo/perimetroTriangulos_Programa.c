#include<stdio.h>
int main(){

int num;
float lado1, lado2, base, perimetro;
system("cls");
printf("Seleccione el tipo de triangulo que desea calcular: \n");
printf("1.- Triangulo Equilatero \n2.- Triangulo Isoceles \n3.- Tiangulo Escaleno\n4.-Salir\n\n");
printf("Escribe una opcion: ");
scanf("%d", &num);

switch (num){

case 1:
    printf("Triangulo Equilatero\n");
system("cls");
printf("Ingresa el valor de un lado del triangulo: ");
scanf("%f",&lado1);

perimetro= lado1 *3;
  if(lado1>0){
printf("El perimetro es: %.2f ", perimetro);
printf("\n\n***El perimetro del triangulo Equilatero tiene todos sus lados iguales con valor (%.2f) ***",lado1);
printf("\n\n %.2f + %.2f + %.2f es igual a %.2f\n",lado1, lado1, lado1, perimetro);
getch();
main();
  }
if(lado1<0){
    printf("Intentalo nuevamente con valores positivos");
    sleep(1);
    system("cls");
main();
}
break;

case 2:
    printf("Triangulo Isoceles\n");
    system("cls");
    printf("Ingresa la base del triangulo: ");
scanf("%f", &base);
printf("Ingresa el valor de un lado del triangulo: ");
scanf("%f",&lado1);

perimetro= lado1 *2 +base;
  if(base>0 && lado1>0){
    system("cls");
printf("El perimetro es: %.2f ", perimetro);
printf("\n\n***El perimetro del triangulo Isoceles tiene dos lados iguales con valor (%.2f) que se suman junto con la base %.2f***",lado1, base);
printf("\n\n %.2f + %.2f + %.2f es igual a %.2f\n",lado1, lado1, base, perimetro);
getch();
main();
  }
if(base<0 || lado1<0){
   system("cls");
    printf("Intentalo nuevamente con valores positivos");
    sleep(1);
    system("cls");
    main();
}
    break;

case 3:
    printf("Triangulo Escaleno");
    system("cls");
    printf("Ingresa la base del triangulo: ");
scanf("%f", &base);
printf("Ingresa el valor de un lado del triangulo: ");
scanf("%f",&lado1);
printf("Ingresa el valor del otro lado del triangulo: ");
scanf("%f",&lado2);

perimetro= lado1 +lado2 +base;
  if(base>0 && lado1>0 && lado2>0){
    system("cls");
printf("El perimetro es: %.2f ", perimetro);
printf("\n\n***El perimetro del triangulo Escaleno es la suma de sus tres lados ***");
printf("\n\n %.2f + %.2f + %.2f es igual a %.2f\n", base, lado1, lado2, perimetro);
getch();
main();
  }
if(base<0 || lado1<0 ||lado2<0){
   system("cls");
    printf("Intentalo nuevamente con valores positivos");
    sleep(1);
    system("cls");
    main();
}
break;

case 4:
    system("cls");
    printf("Adios");
    exit(0);

default: printf("Opcion erronea");
 sleep(1);
    system("cls");
    main();
}
}
